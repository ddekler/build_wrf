#!/bin/bash

# Build WRF and it's dependancies according to:
#  [1]: https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compilation_tutorial.php


## Check required utilities:

function check_util() {
    if [ -z $(which $1) ]; then
    #    echo -e "Found: $1"
    #else
        echo -e "Required utility not found: $1"
        exit 1
    fi
}

declare -a REQUIRED_UTILS=("ar" "awk" "cat" "cp" "cut"
                "expr" "file" "grep" "gzip" "head"
                "hostname" "ln" "ls" "make" "mkdir"
                "mv" "printf" "rm" "sed" "sleep"
                "sort" "tar" "touch" "tr" "uname"
                "wc" "which" "m4" "git")

for UTIL in "${REQUIRED_UTILS[@]}"; do
    check_util $UTIL
done

### Building libraries

function get_tar_name() {
    # Get the filename from a URL
    basename $1
}

function get_dir_name() {
    # Get the directory name from a tar's filename
    echo $1 | sed -e "s/\.tar//g" | sed -e "s/\.gz//g"
}

function build_lib() {
    NAME=$1
    URL=$2
    PREFIX=$3
    CONF_ARGS=$4
    TAR_NAME=$(get_tar_name $URL)
    DIR_NAME=$(get_dir_name $TAR_NAME)

    echo "Building:"
    echo "  $NAME"
    echo "  URL=$URL"
    echo "  PREFIX=$PREFIX"
    echo "  CONF_ARGS=$CONF_ARGS"
    echo "  TAR_NAME=$TAR_NAME"
    echo "  DIR_NAME=$DIR_NAME"

    return

    if [ ! -f $TAR_NAME ]; then
        wget $URL
    fi
    tar xf $TAR_NAME
    cd $DIR_NAME

    ./configure $CONF_ARGS
    make -j 16
    make install
}

## NETCDF

DIR=$PWD
NEW_PATH=

#NETCDF_PREFIX=netcdf
#NETCDF_C_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-c-4.7.2.tar.gz"
#NETCDF_FORTRAN_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/netcdf-fortran-4.5.2.tar.gz"
#
#if [ ! -f $DIR/$NETCDF_PREFIX/bin/nc-config ]; then
#    build_lib "netcdf-c" $NETCDF_C_URL $DIR/$NETCDF_PREFIX "--prefix=$DIR/$NETCDF_PREFIX --disable-dap --disable-netcdf-4 --disable-shared"
#fi
#export NETCDF=$DIR/$NETCDF_PREFIX
#NEW_PATH=$DIR/$NETCDF_PREFIX/bin
#if [ ! -f $DIR/$NETCDF_PREFIX/bin/nf-config ]; then
#    build_lib "netcdf-fortran" $NETCDF_FORTRAN_URL $DIR/$NETCDF_PREFIX "--prefix=$DIR/$NETCDF_PREFIX --disable-dap --disable-netcdf-4 --disable-shared"
#fi

export NETCDF=$(nc-config --prefix)
export LIBS="-lnetcdf -lz"

## MPICH

MPICH_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/mpich-3.0.4.tar.gz"
MPICH_PREFIX=mpich

if [ ! -f $DIR/$MPICH_PREFIX/bin/mpicc ]; then
    build_lib "MPICH" $MPICH_URL $DIR/$MPICH_PREFIX "--prefix=$DIR/$MPICH_PREFIX"
fi

if [ "$MPICH_PREFIX" != "$NETCDF_PREFIX" ]; then
    NEW_PATH=$DIR/$MPICH_PREFIX/bin:$NEW_PATH
fi

## ZLIB

ZLIB_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/zlib-1.2.11.tar.gz"
ZLIB_PREFIX=grib2

if [ ! -f $DIR/$ZLIB_PREFIX/lib/libz.so ]; then
    build_lib "ZLIB" $ZLIB_URL $DIR/$ZLIB_PREFIX "--prefix=$DIR/$ZLIB_PREFIX"
fi

export ZLIB_PATH=$DIR/$ZLIB_PREFIX

## JASPER

JASPER_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/jasper-1.900.1.tar.gz"
JASPER_PREFIX=grib2

if [ ! -f $DIR/$JASPER_PREFIX/bin/jasper ]; then
    build_lib "Jasper" $JASPER_URL $DIR/$JASPER_PREFIX "--prefix=$DIR/$JASPER_PREFIX"
fi

export PATH=$NEW_PATH:$PATH
echo "NEW_PATH=$NEW_PATH"
echo "PATH=$PATH"

export JASPERLIB=$DIR/$JASPER_PREFIX/lib
export JASPERINC=$DIR/$JASPER_PREFIX/include

### Library compatibily tests

TEST_URL="https://www2.mmm.ucar.edu/wrf/OnLineTutorial/compile_tutorial/tar_files/Fortran_C_NETCDF_MPI_tests.tar"
TEST_TAR=$(get_tar_name $TEST_URL)
TEST_DIR=Fortran_C_NETCDF_MPI_tests #$(get_dir_name $TEST_TAR)

echo $TEST_DIR
if [ ! -d $TEST_DIR ]; then
    mkdir $TEST_DIR
fi

cd $TEST_DIR

if [ ! -f $TEST_TAR ]; then
    wget $TEST_URL
fi
if [ ! -d $TEST_DIR ]; then
    tar xf $TEST_TAR
fi

## NOTE: NetCDF-C and NetCDF-Fortran are linked statically and won't show up with ldd

## Test 1: compile with C and Fortran
cp $NETCDF/include/netcdf.inc .
gfortran -c 01_fortran+c+netcdf_f.f
gcc -c 01_fortran+c+netcdf_c.c 
gfortran 01_fortran+c+netcdf_f.o 01_fortran+c+netcdf_c.o -L${NETCDF}/lib -lnetcdff -lnetcdf -o test1
./test1

## Test 2: compile with MPICH
mpif90 -c 02_fortran+c+netcdf+mpi_f.f 
mpicc -c 02_fortran+c+netcdf+mpi_c.c 
mpif90 02_fortran+c+netcdf+mpi_f.o 02_fortran+c+netcdf+mpi_c.o -L${NETCDF}/lib -lnetcdff -lnetcdf -o test2
mpirun -n 2 ./test2

cd ..

### Build WRF

WRF_DIR=WRF
WRF_CASE_NAME=em_les
UBUNTU_RELEASE=$(lsb_release -a | grep "Codename" | cut -f 2)

if [ ! -d $WRF_DIR ]; then
    git clone --recurse-submodules https://github.com/wrf-model/WRF
fi

cd $WRF_DIR

./clean

echo "JASPERLIB=$JASPERLIB"
echo "JASPERINC=$JASPERINC"
./configure <<EOF
35

EOF

./compile -j 16 $WRF_CASE_NAME >& "compile.$UBUNTU_RELEASE.$WRF_CASE_NAME.log"

### Build WPS

#WPS_DIR=WPS
#
#if [ ! -d $WPS_DIR ]; then
#    git clone --recurse-submodules https://github.com/wrf-model/WPS
#fi
#
#cd $WPS_DIR
#
#./clean
#
#./configure <<EOF
#3
#EOF
#
#./compile >& compile.$UBUNTU_RELEASE.log
